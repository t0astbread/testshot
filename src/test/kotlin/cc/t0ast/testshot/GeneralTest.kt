package cc.t0ast.testshot

import org.junit.Assert
import org.junit.Test
import java.io.File
import java.lang.AssertionError

private const val TEST_STRING = "foobar\n\n1234"

private const val DUMMY_FILE_1_PATH = "dummy_file"
private const val DUMMY_FILE_1_CONTENT = "foobar\nin file"
private const val DUMMY_FILE_2_PATH = "dummy_dir/dummy_file"
private const val DUMMY_FILE_2_CONTENT = "foobar\nin file\n\n2"

class GeneralTest {

    @Test
    fun test1() {
        val testFolder = File(System.getProperty("user.dir"), "src/test/resources/test1")

        val snapshotFolder = File(testFolder, "snapshots")
        if(snapshotFolder.exists())
            snapshotFolder.deleteRecursively()
        assert(!snapshotFolder.exists())

        val gitignore = File(testFolder, ".gitignore")
        if(gitignore.exists())
            gitignore.delete()
        assert(!gitignore.exists())

        assertThrows<AssertionError> {
            snapshotTest(testFolder) {
                dummyFunc()
            }
        }

        assert(snapshotFolder.exists())
        assert(snapshotFolder.isDirectory)

        assert(gitignore.exists())
        assert(gitignore.isFile)
        Assert.assertEquals(SNAPSHOT_GITIGNORE_TEXT, gitignore.readText())


        val referenceSnapshotFile = assertSnapshotExists("reference", snapshotFolder)
        assertSnapshotDoesNotExist("last_actual", snapshotFolder)

        snapshotTest(testFolder) {
            dummyFunc()
        }

        assertSnapshotExists("reference", snapshotFolder)
        assertSnapshotExists("last_actual", snapshotFolder)

        File(referenceSnapshotFile, "out").writeText("changed text")

        assertThrows<AssertionError> {
            snapshotTest(testFolder) {
                dummyFunc()
            }
        }
    }

    @Test
    fun testExtraFiles() {
        val testFolder = File(System.getProperty("user.dir"), "src/test/resources/testExtraFiles")

        val snapshotFolder = File(testFolder, "snapshots")
        if(snapshotFolder.exists())
            snapshotFolder.deleteRecursively()
        assert(!snapshotFolder.exists())

        val gitignore = File(testFolder, ".gitignore")
        if(gitignore.exists())
            gitignore.delete()
        assert(!gitignore.exists())

        val dummyFile1 = File(testFolder, DUMMY_FILE_1_PATH)
        if(dummyFile1.exists())
            dummyFile1.delete()
        assert(!dummyFile1.exists())

        val dummyFile2 = File(testFolder, DUMMY_FILE_2_PATH)
        if(dummyFile2.exists())
            dummyFile2.parentFile.deleteRecursively()
        assert(!dummyFile2.exists())

        assertThrows<AssertionError> {
            snapshotTest(testFolder, DUMMY_FILE_1_PATH, DUMMY_FILE_2_PATH) {
                withWorkingDir(testFolder.absolutePath) {
                    dummyFuncWithFiles()
                }
            }
        }

        assert(dummyFile1.exists())
        Assert.assertEquals(DUMMY_FILE_1_CONTENT, dummyFile1.readText())

        assert(dummyFile2.exists())
        Assert.assertEquals(DUMMY_FILE_2_CONTENT, dummyFile2.readText())

        val extraFilesDir = File(snapshotFolder, "reference/extraFiles")
        assert(extraFilesDir.exists())
        assert(extraFilesDir.isDirectory)

        val extraFile1 = File(extraFilesDir, DUMMY_FILE_1_PATH)
        assert(extraFile1.exists())
        assert(extraFile1.isFile)

        val extraFile2 = File(extraFilesDir, DUMMY_FILE_2_PATH)
        assert(extraFile2.exists())
        assert(extraFile2.isFile)

        Assert.assertEquals(dummyFile1.readText(), extraFile1.readText())
        Assert.assertEquals(dummyFile2.readText(), extraFile2.readText())

        snapshotTest(testFolder, DUMMY_FILE_1_PATH, DUMMY_FILE_2_PATH) {
            withWorkingDir(testFolder.absolutePath) {
                dummyFuncWithFiles()
            }
        }

        snapshotTest(testFolder, DUMMY_FILE_1_PATH, DUMMY_FILE_2_PATH, compareFullExtraFileContent = true) {
            withWorkingDir(testFolder.absolutePath) {
                dummyFuncWithFiles()
            }
        }

        snapshotTest(testFolder, DUMMY_FILE_1_PATH, DUMMY_FILE_2_PATH) {
            withWorkingDir(testFolder.absolutePath) {
                dummyFuncWithFiles()
            }
        }

        extraFile1.writeText("changed extra file text")

        assertThrows<AssertionError> {
            snapshotTest(testFolder, DUMMY_FILE_1_PATH, DUMMY_FILE_2_PATH) {
                withWorkingDir(testFolder.absolutePath) {
                    dummyFuncWithFiles()
                }
            }
        }
    }

    private fun assertSnapshotExists(snapshotName: String, snapshotFolder: File): File {
        val snapshotFile = File(snapshotFolder, snapshotName)
        assert(snapshotFile.exists())
        assert(snapshotFile.isDirectory)
        assert(File(snapshotFile, "out").exists())
        assert(File(snapshotFile, "err").exists())
        return snapshotFile
    }

    private fun assertSnapshotDoesNotExist(snapshotName: String, snapshotFolder: File) {
        val snapshotFile = File(snapshotFolder, snapshotName)
        assert(!snapshotFile.exists())
    }

    private fun dummyFunc() {
        println(TEST_STRING)
    }

    private fun dummyFuncWithFiles() {
        println(TEST_STRING)

        File(System.getProperty("user.dir"), DUMMY_FILE_1_PATH).writeText(DUMMY_FILE_1_CONTENT)
        val dummyFile2 = File(System.getProperty("user.dir"), DUMMY_FILE_2_PATH)
        dummyFile2.parentFile.mkdirs()
        dummyFile2.writeText(DUMMY_FILE_2_CONTENT)
    }
}
