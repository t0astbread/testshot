package cc.t0ast.testshot

import java.io.File

class Snapshot(
        var name: String,
        val outStream: Stream,
        val errStream: Stream
) {
    val extraFiles = mutableListOf<String>()

    constructor(name: String, outContent: String, errContent: String) : this(
            name,
            Stream("out", outContent),
            Stream("err", errContent)
    )

    fun store(parentFolder: File) {
        val snapshotFolder = File(parentFolder, this.name)
        storeInSnapshotFolder(snapshotFolder)
    }

    fun store(parentFolder: File, extraFilesBaseFolder: File) {
        val snapshotFolder = File(parentFolder, this.name)
        storeInSnapshotFolder(snapshotFolder)

        val extraFilesFolder = File(snapshotFolder, "extraFiles")
        extraFilesFolder.mkdirs()
        this.extraFiles.forEach { extraFile ->
            storeExtraFile(extraFile, extraFilesBaseFolder, extraFilesFolder)
        }
    }

    private fun storeInSnapshotFolder(snapshotFolder: File) {
        if(snapshotFolder.exists())
            snapshotFolder.deleteRecursively()

        this.outStream.store(snapshotFolder)
        this.errStream.store(snapshotFolder)
    }

    private fun storeExtraFile(extraFilePath: String, extraFilesBaseFolder: File, snapshotExtraFilesFolder: File) {
        val extraFile = File(extraFilesBaseFolder, extraFilePath)
        val fileInSnapshotDir = File(snapshotExtraFilesFolder, extraFilePath)
        extraFile.copyTo(fileInSnapshotDir)
    }

    companion object {
        fun fromSnapshotFolder(snapshotFolder: File): Snapshot {
            val newSnapshot = Snapshot(snapshotFolder.name,
                    Stream.fromFile(File(snapshotFolder, "out")),
                    Stream.fromFile(File(snapshotFolder, "err"))
            )

            val extraFilesDir = File(snapshotFolder, "extraFiles")
            if(extraFilesDir.exists()) {
                extraFilesDir.walkBottomUp()
                    .filter { it.isFile }
                    .map { it.relativeTo(extraFilesDir).toString() }
                    .let { newSnapshot.extraFiles.addAll(it) }
            }

            return newSnapshot
        }
    }
}
