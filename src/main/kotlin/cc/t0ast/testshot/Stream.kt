package cc.t0ast.testshot

import java.io.File

class Stream(
        val name: String,
        val content: String
) {
    fun store(snapshotFolder: File) {
        val streamFile = File(snapshotFolder, this.name)
        streamFile.parentFile.mkdirs()
        streamFile.createNewFile()
        streamFile.writeText(this.content)
    }

    companion object {
        fun fromFile(streamFile: File): Stream {
            return Stream(streamFile.name, streamFile.readText())
        }
    }
}
