package cc.t0ast.testshot

import org.junit.Assert
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.InputStream
import java.io.PrintStream
import java.nio.ByteBuffer
import java.security.MessageDigest
import javax.xml.bind.DatatypeConverter


val SNAPSHOT_GITIGNORE_TEXT = """
        # This file was auto-generated and will be overwritten automatically
        
        # Ignore testshot snapshots
        snapshots/*
        !snapshots/reference
        
    """.trimIndent()


fun snapshotTest(
    testFolder: File,
    vararg extraFiles: String,
    compareFullExtraFileContent: Boolean = false,
    block: () -> Unit
) {
    val snapshotsFolder = File(testFolder, "snapshots")
    snapshotsFolder.mkdirs()

    autoGenerateGitignore(testFolder)

    val actualSnapshot = record("last_actual", block)
    actualSnapshot.extraFiles.addAll(extraFiles)

    val referenceSnapshotFile = File(snapshotsFolder, "reference")
    if (referenceSnapshotFile.exists()) {
        val referenceSnapshot = Snapshot.fromSnapshotFolder(File(snapshotsFolder, "reference"))
        actualSnapshot.store(snapshotsFolder, testFolder)
        assertEquals(referenceSnapshot, actualSnapshot, snapshotsFolder, compareFullExtraFileContent)
    } else {
        actualSnapshot.name = "reference"
        actualSnapshot.store(snapshotsFolder, testFolder)
    }
}

private fun autoGenerateGitignore(testFolder: File) {
    val gitignore = File(testFolder, ".gitignore")

    if (gitignore.exists())
        return

    gitignore.writeText(SNAPSHOT_GITIGNORE_TEXT)
}

fun <T> withInputStream(inputStream: InputStream?, block: () -> T): T {
    if (inputStream == null)
        return block()

    val origIn = System.`in`
    System.setIn(inputStream)

    val result = block()

    System.setIn(origIn)
    return result
}

fun <T> withWorkingDir(wd: String?, block: () -> T): T {
    if (wd == null)
        return block()

    val prevWD = System.getProperty("user.dir")
    System.setProperty("user.dir", wd)

    val result = block()

    System.setProperty("user.dir", prevWD)
    return result
}

fun record(snapshotName: String, block: () -> Unit): Snapshot {
    lateinit var err: String
    val out = recordStream {
        err = recordStream(System.err, { System.setErr(it) }) {
            block()
        }
    }
    return Snapshot(snapshotName, out, err)
}

private fun recordStream(
    origStream: PrintStream = System.out,
    setStream: (PrintStream) -> Unit = { System.setOut(it) },
    block: () -> Unit
): String {
    val recordStream = ByteArrayOutputStream()

    PrintStream(recordStream).use {
        setStream(it)

        block()
    }

    setStream(origStream)
    return recordStream.toString()
}

fun assertEquals(expected: Snapshot, actual: Snapshot, snapshotDir: File? = null, compareFileContent: Boolean = false) {
    Assert.assertEquals(expected.outStream.content, actual.outStream.content)
    Assert.assertEquals(expected.errStream.content, actual.errStream.content)

    if(expected.extraFiles.isNotEmpty() || actual.extraFiles.isNotEmpty()) {
        Assert.assertArrayEquals(expected.sortedExtraFiles(), actual.sortedExtraFiles())
        snapshotDir!!
        if(compareFileContent)
            Assert.assertArrayEquals(expected.extraFileContent(snapshotDir), actual.extraFileContent(snapshotDir))
        else
            Assert.assertArrayEquals(expected.extraFileCheckSums(snapshotDir), actual.extraFileCheckSums(snapshotDir))
    }
}

private fun Snapshot.sortedExtraFiles() =
    this.extraFiles
        .sorted()
        .toTypedArray()

private fun Snapshot.extraFileContent(snapshotDir: File): Array<ByteArray> {
    return sortedExtraFiles()
        .map { extraFilePath -> File(snapshotDir, "$name/extraFiles/$extraFilePath") }
        .map { extraFile -> extraFile.readBytes() }
        .toTypedArray()
}

private fun Snapshot.extraFileCheckSums(snapshotDir: File): Array<String> {
    val messageDigest = MessageDigest.getInstance("MD5")
    return sortedExtraFiles()
        .map { filePath -> filePath to File(snapshotDir, "$name/extraFiles/$filePath") }
        .map { fileData ->
            val file = fileData.second
            messageDigest.reset()
            messageDigest.update(file.readBytes())
            return@map fileData.first to messageDigest.digest()
        }
        .map { hashData -> hashData.first to DatatypeConverter.printHexBinary(hashData.second) }
        .map { hashStrData -> "MD5 of ${hashStrData.first}:\n${hashStrData.second}" }
        .toTypedArray()
}

inline fun <reified E : Throwable> assertThrows(block: () -> Unit) {
    var threw = false
    try {
        return block()
    } catch (thrown: Throwable) {
        if (thrown is E)
            threw = true
    }
    if (!threw)
        Assert.fail("No exception was thrown")
}
